#!/bin/bash

cat system_ext/app/SnapdragonSVA/SnapdragonSVA.apk.* 2>/dev/null >> system_ext/app/SnapdragonSVA/SnapdragonSVA.apk
rm -f system_ext/app/SnapdragonSVA/SnapdragonSVA.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._Kalama_v2_SoC.dtb.* 2>/dev/null >> vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._Kalama_v2_SoC.dtb
rm -f vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._Kalama_v2_SoC.dtb.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null >> vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin
rm -f vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null
cat vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null >> vendor/lib64/libCOSNet_spatial_qnn_quantized.so
rm -f vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null
cat system/system/app/MzAutoInstaller/MzAutoInstaller.apk.* 2>/dev/null >> system/system/app/MzAutoInstaller/MzAutoInstaller.apk
rm -f system/system/app/MzAutoInstaller/MzAutoInstaller.apk.* 2>/dev/null
cat system/system/app/DemonstrationAssistant/DemonstrationAssistant.apk.* 2>/dev/null >> system/system/app/DemonstrationAssistant/DemonstrationAssistant.apk
rm -f system/system/app/DemonstrationAssistant/DemonstrationAssistant.apk.* 2>/dev/null
cat system/system/MzApp/VoiceAssistant/VoiceAssistant.apk.* 2>/dev/null >> system/system/MzApp/VoiceAssistant/VoiceAssistant.apk
rm -f system/system/MzApp/VoiceAssistant/VoiceAssistant.apk.* 2>/dev/null
cat system/system/MzApp/FlymeMusic/FlymeMusic.apk.* 2>/dev/null >> system/system/MzApp/FlymeMusic/FlymeMusic.apk
rm -f system/system/MzApp/FlymeMusic/FlymeMusic.apk.* 2>/dev/null
cat system/system/MzApp/Scanner/Scanner.apk.* 2>/dev/null >> system/system/MzApp/Scanner/Scanner.apk
rm -f system/system/MzApp/Scanner/Scanner.apk.* 2>/dev/null
cat system/system/MzApp/Video/Video.apk.* 2>/dev/null >> system/system/MzApp/Video/Video.apk
rm -f system/system/MzApp/Video/Video.apk.* 2>/dev/null
cat system/system/MzApp/Weather/Weather.apk.* 2>/dev/null >> system/system/MzApp/Weather/Weather.apk
rm -f system/system/MzApp/Weather/Weather.apk.* 2>/dev/null
cat system/system/priv-app/Browser/lib/arm64/libqihoowebview.so.* 2>/dev/null >> system/system/priv-app/Browser/lib/arm64/libqihoowebview.so
rm -f system/system/priv-app/Browser/lib/arm64/libqihoowebview.so.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null >> system/system/priv-app/DirectService/DirectService.apk
rm -f system/system/priv-app/DirectService/DirectService.apk.* 2>/dev/null
cat system/system/priv-app/Picker/Picker.apk.* 2>/dev/null >> system/system/priv-app/Picker/Picker.apk
rm -f system/system/priv-app/Picker/Picker.apk.* 2>/dev/null
cat bootRE/boot.elf.* 2>/dev/null >> bootRE/boot.elf
rm -f bootRE/boot.elf.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null >> product/custom/3rd-party/apk/baidu/baidu.apk
rm -f product/custom/3rd-party/apk/baidu/baidu.apk.* 2>/dev/null
cat product/custom/3rd-party/apk/WPSoffice/WPSoffice.apk.* 2>/dev/null >> product/custom/3rd-party/apk/WPSoffice/WPSoffice.apk
rm -f product/custom/3rd-party/apk/WPSoffice/WPSoffice.apk.* 2>/dev/null
cat product/custom/3rd-party/apk2/Weibo/Weibo.apk.* 2>/dev/null >> product/custom/3rd-party/apk2/Weibo/Weibo.apk
rm -f product/custom/3rd-party/apk2/Weibo/Weibo.apk.* 2>/dev/null
